/* add-on script */
function tabulate(results){
  function sum( obj ) {
    return Object.keys( obj ).reduce( function( sum, key ){
      return sum + parseFloat( obj[key] );
    }, 0 );
  }

  function percentage (total, results){
    var precentageResults = {};
    Object.keys( results ).forEach(function(key){
      if(results[key] === 0){
          precentageResults[key] = 0;
          return;
      } else {
        precentageResults[key] = ((results[key] / total) * 100);
      }
    });
    return precentageResults;
  }

  function sortAssoc (results){
    return Object.keys(results).sort(function(a,b){
      return results[a] - results[b];
    });
  }

  var total = sum(results);
  return {
    total: total,
    sorted: sortAssoc(results),
    percentages: percentage(total, results)
  };
}

function draw(data, onTo, state){
  var i = 0;
  function drawRow(name, value, percentage){
    var result = $('<div class="result" id="row-' + i + '" />'),
    voteButton = $('<input type="radio" id="entry-' + i + '" name="pollentry" />').val(name),
    voteText = $('<label class="votetext" for="entry-' + i + '" />').text(name),
    numeric = $('<span class="aui-badge" id="badge-' + i + '" />').text(value),
    progressIndicator = $('<div class="aui-progress-indicator" />').append('<span class="aui-progress-indicator-value" />');
    voteButton.click(function(){
      if(state === "closed"){
        AJS.messages.warning({
           title: 'Poll Closed',
           body: "<p> Sorry! - no voting after the poll is closed.</p>",
           fadeout: true
        });
      } else {
        castVote($(this).val());
      }
    });
    result.append($('<div class="left" />').append(voteButton).append(voteText));
    result.append($('<div class="right" />').append(numeric).append(progressIndicator));
    return result;
  }
  onTo.empty();
  if (AJS && AJS.progressBars && AJS.progressBars.update) {
      data.sorted.reverse().forEach(function (key) {
          var row = drawRow(key, data.results[key], data.percentages[key]);
          onTo.append(row);
          i++;
          AJS.progressBars.update(row.find(".aui-progress-indicator"), data.percentages[key] / 100);
      });
  }
    else {
      console.log('AJS.progressBars.update does not exist... cannot draw bars!');
  }
}



function signed_request(signed_request){
    if(signed_request){
        $("#signed_request").val(signed_request);
    }
    return $("#signed_request").val();
}

var pathComponents = window.location.pathname.split('/');
var pollId = pathComponents[pathComponents.length - 2];
var clientKey = pathComponents[pathComponents.length - 1];
var pollUrl = '/poll/' + pollId + '/' + clientKey;
var tempPollDetails = document.getElementById('pollId'); // TODO: replace with something that looks nice

var loadingSpinner = $(document.getElementById('loading'));
var votesForm = document.getElementById('votes');
var closePollUrl = pollUrl + '/close';

function getUserKey(){
  if(!document.cookie){
    document.cookie =
    'userkey=' + Date.now() + '; expires=Fri, 3 Aug 2051 20:47:11 UTC; path=/';
  }
  return document.cookie.split('=')[1];
}

function getResults(url){
    return $.ajax({
        url: url,
        headers: {
            'X-acpt': signed_request(),
            'userKey': getUserKey()
        }
    });
}

function castVote(entry){
  $.ajax({
    url: pollUrl,
    type: 'POST',
    data: {
        option: entry,
        userKey: getUserKey() // TODO: make Crappie send it in
    },
    headers: {
        'X-acpt': signed_request()
    },
    success: function (response) {
      init(); // might remove it.
    },
    error: function (response) {
        alert(response.responseText);
    }
  });
}

function renderResults(response){

  if ('closed' == response.state.toLowerCase()) {
      changeUiToClosedPoll()
  }

  var tabulated = tabulate(response.results);
  tabulated.results = response.results;
  $("#total").empty().append('<span>Total votes cast: <span class="aui-badge">' + tabulated.total + '</span></span>');
  draw(tabulated, $("#votes"), response.state);
  if(response.votedFor && response.votedFor.length > 0){
      $('input[value="' + response.votedFor + '"]').attr('checked', true);
  }
}

function init(){
    getResults(pollUrl + '/json').then(function(response){

        var stringified = response ? JSON.stringify(response) : null;

        if (response) {

            if(stringified !== window.lastResponse){
                $("#polltitle").text(response.name);
                // show the heading if we're not inside a dialog with chrome
                if (window.location.search.indexOf('embedded=true') < 0) {
                    $('#polltitle').show();
                }
                renderResults(response);
            }

            $("#pollcontent").show();
            setTimeout(init, 1000);
        }
        else {

            $("#poll-not-found").show();
        }

        window.lastResponse = stringified;
        $("#loading").hide();
    });
}

function changeUiToClosedPoll() {
    $('#close-poll').hide();
    $('#completed-icon').show();
}

function closePoll(){
  changeUiToClosedPoll();
  $.ajax({
      url: closePollUrl,
      type: 'POST',
      data: {
      },
      headers: {
          'X-acpt': signed_request()
      },
      success: function (response) {
        AJS.messages.success({
           title: 'Poll Closed',
           body: "<p>The poll is now closed</p>",
           fadeout: true
        });
      },
      error: function (response) {
          alert(response.responseText);
      }
  });
}




$(function(){
    window.lastResponse = "";
    $("#close-poll").click(function(e){
      e.preventDefault();
      closePoll();
    });
    setTimeout(init, 1000);
});


/* connect all.js loading */

/*global define*/
AC = {};

AC.getBaseUrl = function(){
    return this.getUrlParam('xdm_e', true) + this.getUrlParam('cp', true) + '/atlassian-connect';
};

AC.loadJS = function(url, options){
    var script = document.createElement("script");
    script.src = url;
    if(options){
        script.setAttribute('data-options', options);
    }
    document.getElementsByTagName("head")[0].appendChild(script);
};

AC.getUrlParam = function(param, escape){
    try{
        var regex = new RegExp(param + '=([^&]+)'),
        data = regex.exec(window.location.search)[1];
        // decode URI with plus sign fix.
        return (escape) ? window.decodeURIComponent(data.replace(/\+/g, '%20')) : data;
    } catch (e){
        return undefined;
    }
};

AC.getCapabilities = function(){
    var encoded = window.location.hash.substr(1),
        decoded = decodeURIComponent(encoded);
    try {
        return decoded ? JSON.parse(decoded) : {};
    }
    catch (error) {
        console.log('Error parsing capabilities:', error);
        return {};
    }
};

AC.init = function(){
    AC.capabilities = AC.getCapabilities();
    if (AC.capabilities.hostlib) {
        this.loadJS(AC.capabilities.hostlib, 'margin:false');
    }
};

AC.init();

window.AC = AC;