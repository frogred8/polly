# Polly
### Polls in HipChat
An Atlassian Connect Add-on created using Atlassian Connect Express.

## Developing
Read ACE [docs](https://bitbucket.org/atlassian/atlassian-connect-express/src/master/README.md#markdown-header-install-dependencies) for generic getting-started instructions.

## Running with ngrok
Create a file called `ngrok.json` containing properties:

```
{
    "subdomain": "<your stable ngrok subdomain, e.g. foobah, from https://ngrok.com/dashboard>",
    "authtoken": "<your auth token from https://ngrok.com/dashboard>",
    "port": "<port number of your local process, e.g.3000>"
}
```

* In development mode Polly will automatically find and use these settings, exposing your local dev instance of Polly on `https://yoursubdomain.ngrok.com`.
* Make sure that this file is readable and writeable only by yourself.
* Do not check this file in to source control.

# Data Security and Privacy

In order to provide its services, Polly saves configuration data:

* Poll titles and options entered by you (could be sensitive, depending on what you enter)
* Poll configuration options entered by you (non sensitive)
* Your company's HipChat URL
* System-generated authentication keys

Chat room content and user details are *not* stored by Polly. However, messages containing
slash commands intended for Polly do pass through the add-on temporarily. Polly does not store these.

Data is stored in Postgres databases in Amazon's cloud offering, protected
by username/password credentials. No data is sent to services apart from Polly, HipChat and the
infrastructure used by Polly (currently Amazon).

All communications between Polly and HipChat use HTTPS to prevent eavesdropping.

We have applied conscientious thought to protecting your data security and privacy, with the genuine intention of
["DFTC"](https://www.atlassian.com/company/about/values). We will continue to make our best efforts in this area.

However, this add-on is officially unsupported. You use it at your own risk. We guarantee *nothing*.
We intend to fix any serious problems ASAP, but for a free add-on with no official support you
must acknowledge that "best effort" support provided for free means a lower level of support than for an official product.
There may be bugs of which we are currently unaware and we *may* not fix them immediately upon becoming aware.