/*global process*/
const USAGE = '<br>Usage: <code>/poll [create [for <number> (min | hour | day) "poll name" "option 1" "option 2" ["option 3" [ ... ] ] ]</code>';
const MAX_BAR_HEIGHT = 15;
const CLOSED = 'Closed';
const ONE_SECOND_MILLIS = 1000;
const ONE_HOUR_MILLIS = ONE_SECOND_MILLIS * 60 * 60;
const ONE_WEEK_MILLIS = ONE_HOUR_MILLIS * 24 * 7;

var spawnArgs = require('spawn-args'),
    uuid = require('node-uuid'),
    moment = require('moment'),
    _ = require('lodash'),
    htmlEncode = require('htmlencode'),
    crypto = require('crypto'), // Nodejs encryption with CTR
    symmetricCryptoAlgorithm = 'aes-256-ctr',
    urlObfuscationPassword = 's*qkGHW}vZRVf49uN.nVL6iANADiTx'; // not intended to be super-secret; just to avoid db ids in urls

function encryptSymmetrically(text, password) {

    if (null === text || undefined === text || '' === text) {
        return '';
    }

    var cipher = crypto.createCipher(symmetricCryptoAlgorithm, password);
    var crypted = cipher.update(text, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
}

function obfuscateUrlParam(text) {
    return encryptSymmetrically(text, urlObfuscationPassword);
}

function decryptSymmetrically(ciphertext, password){

    if (null === ciphertext || undefined === ciphertext || '' === ciphertext) {
        return '';
    }

    var decipher = crypto.createDecipher(symmetricCryptoAlgorithm, password);
    var dec = decipher.update(ciphertext, 'hex', 'utf8');
    dec += decipher.final('utf8');
    return dec;
}

function deobfuscateUrlParam(ciphertext) {
    return decryptSymmetrically(ciphertext, urlObfuscationPassword);
}

module.exports = function(app, addon) {
    var hipChat = require('../lib/hipchat')(addon);

    function htmlHeightForNumberOfOptions(num) {
        return (70 + num * 60) + 'px'; // from inspection using Chrome developer tools
    }

    // assumes that a poll is:
    // {
    //   id: 'string',
    //   name: 'string'
    // }
    function createLinkToPoll(req, poll, linkBody) {
        if (linkBody === undefined) {
            linkBody = poll.name;
        }
        var targetOptions = {
            header: poll.name,
            height: htmlHeightForNumberOfOptions(poll.options.length),
            resize: true
        };
        return '<a href="'
            + addon.config.localBaseUrl() + '/poll/' + obfuscateUrlParam(poll['id']) + '/' + obfuscateUrlParam(req.clientInfo.clientKey) + '?height=' + targetOptions.height + '"'
            + ' data-target="dialog"'
            + ' data-target-options="' + htmlEncode.htmlEncode(JSON.stringify(targetOptions)) + '"'
            + '>'
            + linkBody + '</a>';
    }

    // Root route. This route will serve the `addon.json` unless a homepage URL is
    // specified in `addon.json`.
    app.get('/',
        function(req, res) {
            // Use content-type negotiation to choose the best way to respond
            res.format({
                // If the request content-type is text-html, it will decide which to serve up
                'text/html': function() {
                    res.redirect(addon.descriptor.links['homepage']);
                },
                // This logic is here to make sure that the `addon.json` is always
                // served up when requested by the host
                'application/json': function() {
                    res.redirect('/atlassian-connect.json');
                }
            });
        }
    );

    // This is an example route that's used by the default for the configuration page
    app.get('/config',
        // Authenticates the request using the JWT token in the request
        addon.authenticate(),
        function(req, res) {
            // The `addon.authenticate()` middleware populates the following:
            // * req.clientInfo: useful information about the add-on client such as the
            //   clientKey, oauth info, and HipChat account info
            // * req.context: contains the context data accompanying the request like
            //   the roomId
            res.render('config', req.context);
        }
    );

    var images = {
        star: 'https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/goldstar-1417755861.png',
        paddlin: 'https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/paddlin-1417756794.png',
        vote: addon.config.localBaseUrl() + '/img/vote.png',
        voteClosed: addon.config.localBaseUrl() + '/img/vote-closed.png'
    };

    function parseArgs(argsString) {
        var args = [];
        var rawArgs = spawnArgs(argsString) || []; // appending "|| []" resolves an intellij warning about rawArgs.forEach not being defined

        rawArgs.forEach(function(rawArg) {
            args.push(rawArg.replace(/^"/, '').replace(/"$/, ''));
        });

        return args;
    }

    // returns a promise to an array of polls
    function loadPolls(clientInfo) {
        var clientKey = clientInfo.clientKey;
        return addon.settings.get(getSettingsKey(clientInfo), clientKey).then(function(polls) {
            try {
                // console.log('[', clientKey, ']', 'Loaded polls JSON', typeof polls, 'from db: ', polls);
                return polls || [];
            } catch (error) {
                console.log('Unable to parse polls JSON loaded from db: <', polls, '>. Returning no polls.', error);
                return [];
            }
        });
    }

    function getSettingsKey(clientInfo) {
        return 'polls:' + clientInfo.roomId;
    }

    // returns a promise
    function savePolls(clientKey, polls) {

        var pollsJson = JSON.stringify(polls);
        console.log('[', clientKey, ']', 'Saving polls JSON: ', pollsJson);
        return addon.settings.get('clientInfo', clientKey).then(function(clientInfo) {
            addon.settings.set(getSettingsKey(clientInfo), pollsJson, clientKey);
        });
    }

    function getRelativeHeight(poll, option) {
        var maxVotes = Math.max.apply(Math, poll.options.map(function(option) {
            return option.votes.length;
        }));
        if (maxVotes == 0) {
            return 0;
        }
        return Math.floor(MAX_BAR_HEIGHT * option.votes.length / maxVotes);
    }

    function getImageTag(image, altText, height, width) {
        return '<img src="' + image + '" alt="' + altText + '" height="' + height + '" width="' + width + '"/> ';
    }

    function pollSort(a, b) {
        if (a.state == b.state) {
            if (a.created > b.created) {
                return 1;
            } else {
                return -1;
            }
        }
        if (a.state == "Open") {
            return 1;
        } else {
            return -1;
        }
    }

    function getDuration(unitArg, duration, durationArg) {
        var units;
        if (/(sec|secs|seconds)/.test(unitArg)) {
            units = 'seconds';
        } else if (/(min|mins|minutes)/.test(unitArg)) {
            units = 'minutes';
        } else if (/(hour|hours)/.test(unitArg)) {
            units = 'hours';
        } else if (/(day|days)/.test(unitArg)) {
            units = 'days';
        }
        duration = moment.duration(durationArg, units);
        return duration;
    }

    function getWinningVoteCount(poll) {
        return Math.max.apply(Math, poll.options.map(function(option) {
            return option.votes.length;
        }));
    }

    function sendCloseNotification(req, poll) {
        var clientInfo = req.clientInfo;
        hipChat.sendMessage(clientInfo, poll.roomId, getPollMessage(req, poll));
    }

    function scheduleReminders(req, poll, duration) {
        var halfwayPoint = duration / 2;
        var paddlingPoint = duration - 10000;

        var clientInfo = req.clientInfo;
        var clientKey = clientInfo.clientKey;

        setTimeout(function() {
            addon.settings.get(getSettingsKey(clientInfo), clientKey).then(function(polls) {
                polls.forEach(function(poll2) {
                    if (poll2.id == poll.id) {
                        var notificationMessage = getPollMessage(req, poll2) + "&nbsp;&nbsp;&nbsp;" + "Polling closes soon, don't forget to vote!";
                        hipChat.sendMessage(req.clientInfo, req.context.item['room'].id, notificationMessage);
                    }
                });
                var pollsJson = JSON.stringify(polls);
                addon.settings.set(getSettingsKey(clientInfo), pollsJson, clientKey);
            });
        }, halfwayPoint);

        if (paddlingPoint > halfwayPoint) {
            setTimeout(function() {
                addon.settings.get(getSettingsKey(clientInfo), clientKey).then(function(polls) {
                    polls.forEach(function(poll2) {
                        if (poll2.id == poll.id) {
                            var notificationMessage = getPollMessage(req, poll2) + "&nbsp;&nbsp;&nbsp;" + "Vote now, or it's time for a " + createLinkToPoll(req, poll, getImageTag(images.paddlin, "Paddlin!", 25, 25));
                            hipChat.sendMessage(req.clientInfo, req.context.item['room'].id, notificationMessage);
                        }
                    });
                    var pollsJson = JSON.stringify(polls);
                    addon.settings.set(getSettingsKey(clientInfo), pollsJson, clientKey);
                });
            }, paddlingPoint);
        }

        setTimeout(function() {
            // There's a hack with a lot of duplication in here versus loadPolls/savePolls.  Ran out of time to clean it up.

            addon.settings.get(getSettingsKey(clientInfo), clientKey).then(function(polls) {
                    polls.forEach(function(poll2) {
                        if (poll2.id == poll.id) {
                            poll2.state = CLOSED;
                            sendCloseNotification(req, poll2);
                        }
                    });
                    var pollsJson = JSON.stringify(polls);
                    addon.settings.set(getSettingsKey(clientInfo), pollsJson, clientKey);
                },
                function(error) {
                    console.log('ERROR:', error);
                });
        }, duration);
    }

    function getOptionsMessage(poll) {
        var sortedOptions = poll.options.sort(function(a, b) {
            return b.votes.length - a.votes.length;
        });

        var winningCount = getWinningVoteCount(poll);

        var voteRows = [];

        _.each(sortedOptions, function(option) {
            var row = "<tr>";
            var voteCount = option.votes.length;

            var img = "";
            if (winningCount > 0 && winningCount === voteCount) {
                img = getImageTag(images.star, 'Winner', 16, 16);
            }

            row += "<td>" + img + "&nbsp;</td>";
            row += "<td>&nbsp;" + option.name + "&nbsp;</td>";
            row += "<td>&nbsp;(" + voteCount + " vote" + (voteCount === 1 ? '' : 's') + ")&nbsp;</td>";

            var stars = _.padRight(_.repeat('*', getRelativeHeight(poll, option)), MAX_BAR_HEIGHT);

            row += "<td><code>" + stars + "</code></td>";
            row += "</tr>";

            voteRows.push(row);
        });

        return voteRows.join("");
    }

    function getPollMessage(req, poll) {
        var imageTag = poll.state === 'Open' ?
            getImageTag(images.vote, 'Vote!', 64, 64) :
            getImageTag(images.voteClosed, 'Voting closed', 64, 64);

        var pollMessage =
            "<table>" +
            "  <tr>" +
            "     <td rowspan=" + (poll.options.length + 1) + ">" + imageTag + "</td>" +
            "     <td colspan='4'><b>" + createLinkToPoll(req, poll) + "</b></td>" +
            "  </tr>";

        pollMessage += getOptionsMessage(poll);
        pollMessage += "</table>";

        console.log(pollMessage);

        return pollMessage;
    }

    function buildWebhookReplyMessage(req, args, polls) {

        var message = '';
        var firstArg = args.shift();

        if (firstArg) {

            if ('create' == firstArg) {

                var usageCheckFailed = false;
                var duration = null;
                var secondArg = args[0];
                if (secondArg == "for") {
                    var thirdArg = args[1];
                    var fourthArg = args[2];
                    if (/(\d+)/.test(thirdArg) && /(min|mins|minutes|sec|secs|seconds|hour|hours|day|days)/.test(fourthArg)) {
                        args.shift();

                        var durationArg = parseInt(args.shift(), 10);
                        var unitArg = args.shift();
                        duration = getDuration(unitArg, duration, durationArg);
                    } else {
                        usageCheckFailed = true;
                    }
                }

                var subCommandArgs = args;

                console.log('create: ', subCommandArgs);

                if (usageCheckFailed) {
                    message = '<create> for must be followed by a number and min/hour/day' + USAGE;
                } else if (subCommandArgs && subCommandArgs.length >= 3) {
                    var optionNames = subCommandArgs.splice(1);
                    var options = optionNames.map(function(optionName) {
                        return {
                            name: optionName,
                            votes: []
                        };
                    });
                    //options[0].votes = ["Voter #3", "Voter #4", "Voter #5", "Voter #6", "Voter #7", "Voter #8" ];
                    //options[1].votes = ["Voter #3a", "Voter #4a", "Voter #5a", "Voter #6a", "Voter #7a", "Voter #8a" ];
                    //if (options[0].length > 2) {
                    //    options[2].votes = ["Voter #A"];
                    //}

                    var poll = {
                        id: uuid.v4(),
                        name: subCommandArgs[0],
                        options: options,
                        state: "Open",
                        roomId: req.context.item['room'].id,
                        created: new Date(),
                        updated: new Date()
                    };

                    polls.push(poll);
                    savePolls(req.clientInfo.clientKey, polls); // asynchronous; assume that it worked
                    message += getPollMessage(req, poll);
                    if (duration) {
                        scheduleReminders(req, poll, duration);
                    }
                } else {
                    message = '<create> needs to be followed by at least three arguments!' + USAGE;
                }
            } else {
                message = 'Unknown argument: "' + firstArg + '".' + USAGE;
            }
        } else {
            if (polls.length > 0) {
                message = '';
                var sortedPolls = polls.sort(pollSort);
                sortedPolls.forEach(function(poll) {
                    message += getPollMessage(req, poll) + "<br>";
                });
            } else {
                message += 'No open polls! Why not create one?' + USAGE;
            }
        }

        return message;
    }

    app.post('/webhook',
        addon.authenticate(),
        function(req, res) {

            var argsString = req.body.item.message.message.replace(/^\/poll ?/, '');
            var args = parseArgs(argsString);
            loadPolls(req.clientInfo).then(function(polls) {
                var options = {
                    message: buildWebhookReplyMessage(req, args, polls),
                    "color": "purple",
                    "notify": true,
                    "message_format": "html"
                };
                res.json(options);
            });

        }
    );

    // get a json representation of this poll
    app.get('/poll/:id/:clientKey/json',
        addon.checkValidToken(), // TODO: provide a nicer response if the auth token is missing or expired
        function(req, res) {

            var clientKey = deobfuscateUrlParam(req.params['clientKey']);
            addon.settings.get('clientInfo', clientKey).then(function(clientInfo) {
                loadPolls(clientInfo).then(function(polls) {

                    var found = false;
                    var pollId = deobfuscateUrlParam(req.params['id']);

                    polls.forEach(function(poll) {
                        if (poll.id == pollId) {
                            var data = {
                                name: poll.name,
                                results: {},
                                votedFor: '',
                                state: poll.state.toLocaleLowerCase()
                            };
                            poll.options.forEach(function(option) {
                                data.results[option.name] = option.votes.length;
                                if (option.votes.indexOf(req.headers['userkey']) >= 0) {
                                    data.votedFor = option.name;
                                }
                            });
                            res.json(data);
                            found = true;
                        }
                    });

                    if (!found) {
                        res.json(null);
                    }
                });
            });
        }
    );

    // Create a JWT token that can be used instead of a session cookie
    function createSessionToken(verifiedClaims, clientKey, secret) {
        var now = moment().utc();
        //noinspection JSCheckFunctionSignatures
        return addon._jwt.encode({
            'iss': addon.key,
            'sub': verifiedClaims.sub,
            'iat': now.unix(),
            'exp': now.add(addon.config.maxTokenAge(), 'seconds').unix(),
            'aud': [clientKey]
        }, secret);
    }

    // get html to go inside the dialog
    app.get('/poll/:id/:clientKey',
        //addon.authenticate(), // TODO: when Patrick has come up with an authentication solution (also replace addon._jwt usage above)
        function(req, res) {

            var clientKey = deobfuscateUrlParam(req.params['clientKey']);

            if (clientKey) {
                addon.settings.get('clientInfo', clientKey).then(function(clientInfo) {

                    if (clientInfo) {
                        res.render('poll', {
                            signed_request: createSessionToken({}, clientKey, clientInfo.sharedSecret),
                            initialHeight: req.query.height || htmlHeightForNumberOfOptions(2) // there have to be at least 2 options
                        });
                    } else {
                        console.log('No such clientKey in our database: "' + clientKey + '".');
                        res.render('no-id');
                    }
                });
            } else {
                console.log('Received null clientKey. Sending no-such-poll response.');
                res.render('no-id');
            }
        }
    );

    // whoops, someone constructed a url with the path ending in '/poll' instead of '/poll/some-id'
    app.get('/poll',
        // no auth necessary; this is a broken url
        function(req, res) {
            res.render('no-id');
        }
    );

    // submit a vote
    app.post('/poll/:id/:clientKey',
        addon.checkValidToken(),
        function(req, res) {
            var pollId = deobfuscateUrlParam(req.params['id']);
            var clientKey = deobfuscateUrlParam(req.params['clientKey']);
            var userKey = req.body.userKey;
            var optionName = req.body.option;
            //console.log(clientKey, pollId, userKey, optionName);

            addon.settings.get('clientInfo', clientKey).then(function(clientInfo) {

                loadPolls(clientInfo).then(function(polls) {

                    var found = false;
                    var changed = false;

                    polls.forEach(function(poll) {
                        if (poll.id == pollId) {
                            poll.options.forEach(function(option) {
                                if (option.name == optionName) {
                                    if (option.votes.indexOf(userKey) >= 0) {
                                        found = true;
                                    } else {
                                        option.votes.push(userKey);
                                        console.log('Added vote. (', poll.name, '->', option.name, ') now has these votes:', option.votes);
                                        changed = true;
                                        found = true;
                                    }
                                } else {
                                    var index = option.votes.indexOf(userKey);
                                    if (index >= 0) {
                                        changed = true;
                                        option.votes.splice(index, 1);
                                        console.log('Removed vote. (', poll.name, '->', option.name, ') now has these votes:', option.votes);
                                    }
                                }
                            });
                        }
                    });

                    if (found) {
                        if (changed) {
                            savePolls(clientKey, polls);
                        }
                        res.send(200);
                    } else {
                        res.send(400, 'No such poll or option');
                    }
                });
            });
        }
    );

    app.post('/poll/:id/:clientKey/close',
        addon.checkValidToken(),
        function(req, res) {
            var pollId = deobfuscateUrlParam(req.params['id']);
            var clientKey = deobfuscateUrlParam(req.params['clientKey']);
            //console.log(clientKey, pollId, userKey, optionName);

            addon.settings.get('clientInfo', clientKey).then(function(clientInfo) {

                loadPolls(clientInfo).then(function(polls) {

                    var found = false;
                    var changed = false;

                    polls.forEach(function(poll) {
                        if (poll.id == pollId) {
                            found = true;
                            if (poll.state = "Open") {
                                poll.state = CLOSED;
                                changed = true;
                                req.clientInfo = clientInfo;
                                sendCloseNotification(req, poll);
                            }
                        }
                    });

                    if (found) {
                        if (changed) {
                            savePolls(clientKey, polls);
                        }
                        res.send(200);
                    } else {
                        res.send(400, 'No such poll or option');
                    }
                });
            });
        }
    );

    // Notify the room that the add-on was installed
    addon.on('installed', function(clientKey, clientInfo, req) {
        hipChat.sendMessage(clientInfo, req.body.roomId, 'The ' + addon.descriptor.name + ' add-on has been installed in this room. Type "/poll" to start polling your team.');
        setTimeout(function() {
            clientInfo.sharedSecret = uuid.v4();
            clientInfo.baseUrl = clientInfo.capabilitiesDoc.links['homepage']; // otherwise our authentication middleware blows up
            addon.settings.set('clientInfo', clientInfo, clientKey); // TODO: remove when we have Patrick's authentication solution
        }, 1000);
    });

    // Clean up clients when uninstalled
    addon.on('uninstalled', function(id) {
        console.log('Uninstalled from tenant:', id);
        addon.settings.client.keys(id + ':*', function(err, rep) {
            rep.forEach(function(k) {
                addon.logger.info('Removing key:', k);
                addon.settings.client.del(k);
            });
            addon.settings.del('clientInfo', id);
        });
    });

    // delete polls that have been closed for "too long"
    function deleteOldClosedPolls() {

        console.log('Looking for old, closed polls to delete...');
        var now = new Date();

        addon.settings.getAllClientInfos().then(function(clientInfos) {

            clientInfos.forEach(function(clientInfo) {
                var pollsKey = getSettingsKey(clientInfo);

                addon.settings.get(pollsKey, clientInfo.clientKey).then(function(polls) {
                    if (polls) {
                        var newPolls = [];
                        polls.forEach(function(poll) {
                            var lastUpdated = new Date(poll.updated);

                            // delete if closed && 'too old'
                            if (CLOSED == poll.state && now.getTime() - lastUpdated.getTime() > ONE_WEEK_MILLIS) {
                                console.log('[' + clientInfo.clientKey + '] Deleting closed poll "' + poll.name + '" because it was last updated on ' + lastUpdated.toDateString());
                            } else {
                                console.log('[' + clientInfo.clientKey + '] Poll is open or recently updated: "' + poll.name + '", updated ', lastUpdated);
                                newPolls.push(poll);
                            }
                        });

                        var numPollsToDelete = polls.length - newPolls.length;

                        if (numPollsToDelete > 0) {
                            console.log('[' + clientInfo.clientKey + '] Deleting', numPollsToDelete, 'poll(s).');
                            addon.settings.set(pollsKey, newPolls, clientInfo.clientKey);
                        }
                    }
                });
            });

            // repeat once per hour
            setTimeout(deleteOldClosedPolls, ONE_HOUR_MILLIS);
        });
    }

    // start deleting polls that have been closed for "too long"
    setTimeout(deleteOldClosedPolls, ONE_SECOND_MILLIS);

    app.get('/healthcheck', function(req, res) {
        res.send(200);
    });

    app.get('/stats', function(req, res) {
        addon.settings.getAllClientInfos().then(function(clientInfos) {

            var numTenants = clientInfos ? clientInfos.length : 0;
            var clientsRemaining = numTenants;
            var numPolls = 0;

            var respond = function(res, numTenants, numPolls) {
                res.json({
                    numTenants: numTenants,
                    numPolls: numPolls
                });
            };

            if (clientInfos && clientInfos.length > 0) {
                clientInfos.forEach(function (clientInfo) {
                    addon.settings.get(getSettingsKey(clientInfo), clientInfo.clientKey).then(function (polls) {
                        numPolls += polls ? polls.length : 0;
                        --clientsRemaining;

                        if (clientsRemaining <= 0) {
                            respond(res, numTenants, numPolls);
                        }
                    });
                });
            }
            else {
                respond(res, 0, 0);
            }
        });
    });
};